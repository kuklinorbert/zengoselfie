library zengoquiz;

import 'package:flutter/material.dart';

class ZengoQuiz extends StatefulWidget {
  final ValueSetter<String> onResult;
  const ZengoQuiz({required this.onResult, Key? key}) : super(key: key);

  @override
  State<ZengoQuiz> createState() => _ZengoQuizState();
}

class _ZengoQuizState extends State<ZengoQuiz> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
